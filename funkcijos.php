<?php

// Globali sritis (Global scope)
$labas = "rytas";



function sudetis($a, $b) {
	// Lokali sritis (Local scope)
	$rezultatas = $a + $b;

	return $rezultatas;
}


$skaicius1 = 6;
$skaicius2 = 7;
echo sudetis($skaicius1, $skaicius2);

$skaicius2++;
echo sudetis($skaicius2, $skaicius1);

