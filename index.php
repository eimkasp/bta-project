<?php
session_start();

include( 'projects.php' ); ?>
<?php
var_dump($_SESSION);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<?php if(isset($_SESSION['vardas']))  {
    echo "Sveiki, " . $_SESSION['vardas'];

}

?>
<?php

// Skaiciuojame pasiriinktu checkbox projektu bendra suma
if ( isset( $_GET['projects'] ) ) {
	$suma       = 0;
	$projectsID = $_GET['projects'];

	foreach ( $projectsID as $project ) {
		$suma += $projects[ $project ]['price'];
	}
}





function cmp_all( $a, $b ) {

    // pasiemame get parametra sort, pagal kuri masyvo laukeli rikiuojamas masyvas
    $sort = $_GET['sort']; // year , price

    // rikiavimo tvarka, didejanti arba mazejanti
    $order = $_GET['order']; // asc , desc


    // jei didejimo tvarka, tai naudojam viena rikiavima, jei mazejimo, kita
    if($order == 'asc') {
		if ( $a[$sort] > $b[$sort] ) {
			return -1;
		} else if ( $a[$sort] < $b[$sort] ) {
			return 1;
		} else {
			return 0;
		}
    } else {
		if ( $a[$sort] > $b[$sort] ) {
			return 1; // vietoj -1 graziname 1
		} else if ( $a[$sort] < $b[$sort] ) {
			return -1;
		} else {
			return 0;
		}
    }

}
// iskvieciame rikavimo funkcija
usort( $projects, 'cmp_all' );



// patikriname dabartine order reiksme
if(isset($_GET['order'])) {
	$sortDirection = $_GET['order'];
} else {

    // jei order reiksme
    // neegzistuoja nustatmoe standartine reiksme asc
	$sortDirection = 'asc';
}

// apkeiciame sortDirection reiksme i atvirkstine,
//kad paspaude antra karta, gautume atvirkstini rikiavima
if($sortDirection == 'asc') {
    $sortDirection = 'desc';
} else if($sortDirection == 'desc') {
    $sortDirection = 'asc';
}

?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <form>
                <h1>Projektai</h1>

                <div class="row">
                    <div class="col-6">
                        <input type="submit" class="btn btn-primary mb-3" value="Skaiciuoti">
                    </div>

                    <div class="col-6">
						<?php if ( isset( $_GET['projects'] ) ) : ?>
                            Pasirinktu projektu suma yra: <?php echo $suma; ?>€
						<?php endif; ?>
                    </div>
                </div>


                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>
                            <a href="?sort=short_name&order=<?php echo $sortDirection; ?>">
                                Sutrumpinimas
                            </a>
                        </th>
                        <th>
                            <a href="?sort=year&order=<?php echo $sortDirection; ?>">
                                Metai
                            </a>
                        </th>
                        <th>
                            <a href="?sort=program&order=<?php echo $sortDirection; ?>">
                                Programa
                            </a>
                        </th>
                        <th>
                            <a href="?sort=price&order=<?php echo $sortDirection; ?>">
                                Suma
                            </a>
                        </th>
                    </tr>
					<?php foreach ( $projects as $key => $project ) : ?>
                        <tr>
                            <td>
                                <!-- patikriname ar musu projektas buvo get['projects'] masyve-->
								<?php if ( in_array( $key, $_GET['projects'] ) ) : ?>
                                    <!-- jei taip, tai uzdedame checked atributa -->
                                    <input type="checkbox" checked name="projects[]" value="<?php echo $key; ?>">
								<?php else: ?>
                                    <input type="checkbox" name="projects[]" value="<?php echo $key; ?>">
								<?php endif; ?>
                            </td>
                            <td><?php echo $project['short_name']; ?></td>
                            <td><?php echo $project['year']; ?></td>
                            <td><?php echo $project['program']; ?></td>
                            <td><?php echo $project['price']; ?>€</td>
                        </tr>
					<?php endforeach; ?>
                </table>
            </form>


        </div>
    </div>
</div>

</body>
</html>